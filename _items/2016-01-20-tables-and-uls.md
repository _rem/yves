---
description: > # "title:"
  Sample item with table and list
title: "Item's tables and lists"
---

## Sample table with inline code

| Description | Command |
|---------|----|
| \<old> word for \<new> | `:s/<old>/<new>` |
| Every \<old> word for \<new> in a line | `:s/<old>/<new>/g` |
| Every \<old> for \<new> from \<ln1> to \<ln2> | `:<ln1>,<ln2>s/<old>/<new>/g` |
| Every \<old> for \<new> in whole buffer | `:%s/<old>/<new>/g` |
| (As above, confirm each time) | `:%s/<old>/<new>/gc` |

## Sample unordered list

* Something incredible is waiting to be known.
* Apple pies the sky calls to us.
* Imagination, stirred by starlight ship of the imagination.
* Extraplanetary ship of the imagination.
* paroxysm of global death!
