---
description: > # "title:"
  Key web typography rules.
title: "Web Typography"
---

The typographic quality of a document is largely determined by how the body text
looks. Keep the following points in mind for a better body text.

- Point size. 15-25 pixels.
- Line spacing. 120-145% of the point size. Modify the `line-height` in CSS.
- Line length. 66 characters, ideal. Anywhere from 45 to 85 characters is good.
- System Fonts. Avoid using Times New Roman, and Arial.
- Monospaced Fonts. Only use them for code.
- Serif fonts. Only on higher resolution screens.
- Word spaces. Never use them for styling. Use `&thinsp;` for acronyms.
- Underlining. Don't use it unless it's a hyperlink.
- Bold and Italics. Use as little as possible.
- Small caps. Only if actually available. Use an extra 5-12% of letter spacing.
- First-line indents or paragraph spaces, never both.
- Fist-line indent. 1-4x the text point size.
- Paragraph space. 4-10 points of space between paragraphs.
- Use hyphenation in justified text
- Use curly quotes.

## Typographic Elements

<style>
  td, th { text-align:center; }
</style>

| Character | OS X                 | HTML       |
|-----------|----------------------|------------|
| '	     | `'`                  | `'`        |
| "	     | `"`                  | `"`        |
| &lsquo;   | option + `]`         | `&lsquo;`  |
| &rsquo;   | option + shift + `]` | `&rsquo;`  |
| &ldquo;   | option + `[`         | `&ldquo;`  |
| &rdquo;   | option + shift + `[` | `&rdquo;`  |
| &ndash;   | option + `-`         | `&ndash;`  |
| &mdash;   | option + shift + `-` | `&mdash;`  |
| &hellip;  | option + `;`         | `&hellip;` |
| &amp;  | &                    | `&amp;`  |
| &copy;    | option + `g`         | `&copy;`   |
| &cent;    | option + `4`         | `&cent;`   |
| &pound; | option + `3`         | `&pound;`  |
| &euro; | option + shift + `2` | `&euro;` |


## Resources

For more elements check out:

- [HTML5 Character Entity Reference Chart](https://dev.w3.org/html5/html-author/charref)
- [HTML5 Entity Tables](https://www.tutorialspoint.com/html5/html5_entities.htm)
- [Typewolf's Typography Cheatsheet](https://www.typewolf.com/cheatsheet)
- [HTML Symbols](https://www.w3schools.com/charsets/ref_utf_math.asp)
