# Yves

A minimalist mobile-first Jekyll theme with responsive
[fonts](https://websemantics.uk/articles/responsive-fonts/demo/).
Yves supports
[Tomorrow Night Blue](https://atom.io/themes/tomorrow-night-blue-syntax)
colors for code blocks.

**Beware:** Due to [Github Pages requirements](https://pages.github.com/versions/)
Yves currently can't be deployed there.
Instead, you can use Gitlab's pages which only require to upload the site already
built. The caveat being you'll need a `gitlab-ci.yml`.
Check [Search & Deploy's gitlab-ci.yml](https://notabug.org/rem/search-and-deploy/src/master/.gitlab-ci.yml)
for a live example.
You can also upload the already built site to [neocities](https://neocities.org/).

## Get Started

- Clone this repo.
- Run `bundle install -j4 --path .bundle/gems` to install all dependencies locally.
- Create a `_drafts` folder to keep your unpublished posts in.
- Run Yves locally to check it out `bundle exec jekyll serve`.

## Publications

Yves is designed to manage a blog, a micro blog, and a collection of thematic posts.
Blog and micro blog posts are included in the RSS feed. Therefore, are publish
through the `_posts` directory. Collection items go inside `_items`. Check out
the sample files for front matter examples.

## Contributions

Security and privacy related suggestions, patches, feature requests, and/or their
implementations are welcomed at the [oficial repo](https://notabug.org/rem/yves).

In the spirit of encouraging open source development. This software is meant to
be modified, and openly maintained by yourself. Read the
[LICENSE](https://choosealicense.com/licenses/artistic-2.0/).
Fork the repo. Hack on!

## License

Copyright &copy; René Maya

Artistic License 2.0

For a full copy of the text take a look at the
[LICENSE](https://notabug.org/rem/yves/src/master/LICENSE) file.

You are always permitted to make arrangements wholly outside of this license
directly with the Copyright Holder. If the terms of this license do not permit
the full use that you propose to make of this software and associated
documentation files, you should contact the Copyright Holder and seek a different
licensing arrangement.
