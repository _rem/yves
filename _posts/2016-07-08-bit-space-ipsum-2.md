---
title: space ipsum 2
categories: bits
---
Buy why, some say, the moon? Why choose this as our goal? And they may as well ask why climb the highest mountain?

Space, the final frontier. These are the voyages of the Starship Enterprise. Its five-year mission: to explore strange new worlds, to seek out new life and new civilizations, to boldly go where no man has gone before.

We have an infinite amount to learn both from nature and from each other
