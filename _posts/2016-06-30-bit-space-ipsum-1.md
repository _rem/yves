---
title: Space Ipsum 1
categories: bits
---
If you could see the earth illuminated when you were in a place as dark as night, it would look to you more splendid than the moon.

```conf
[diff]
  algorithm = histogram
```
