---
title: Space Ipsum 3
categories: bits
---
Spaceflights cannot be stopped. This is not the work of any one man or even a group of men. It is a historical process which mankind is carrying out in accordance with the natural laws of human development.

We are all connected; To each other, biologically. To the earth, chemically. To the rest of the universe atomically.

Here men from the planet Earth first set foot upon the Moon. July 1969 AD. We came in peace for all mankind.
