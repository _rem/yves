---
title: lorem ipsum code blocks
categories: bits
---

```ruby
class MyClassTest < Minitest::Test
  def setup
    @test_object = Object.new
    @test_object.extend(Mixable) # has a 'mix?' method
  end

  def test_mixes_correctly
    assert @test_object.mix?
  end
end
```

```ruby
class DummyObject
  include OtherMixable # its 'mix' method depends on 'necessary_method'

  def necessary_method
    # your sample implementation
  end
end

class MyClassTest < Minitest::Test
  def test_my_object_mixes
    my_object = DummyObject.new
    assert my_object.mix
  end
end
```
