---
title: Changelog
description: >
  Yves Chagelog
categories: posts
tags: [history, open-source]
---

<div class="centered-image">
  <img alt="Shot of Yves Klein's IKB 191" src="{{ site.url }}/images/IKB_191.jpg">
</div>

2017-08-14: Publish Yves on its own [repo](https://www.github.com/renemaya/yves).

2017-07-24: Publish Yves as part of [Search & Deploy](https://search-and-deploy.netlify.com).

## Resources

These are some of the ideas that influenced Yves.

- [Keeping CSS specificity low](https://css-tricks.com/strategies-keeping-css-specificity-low/)
- [Code Guide by @mdo](http://codeguide.co/)
- [Object Oriented CSS](https://github.com/stubbornella/oocss/wiki/faq)
- [Principles of writing consistent, idiomatic CSS](https://github.com/necolas/idiomatic-css)
- [CSS purge](http://www.csspurge.com/)
- [Semantics and Accessibility](http://accessiblehtmlheadings.com/)
- [HTML5 Semantic Elements](https://www.w3schools.com/html/html5_semantic_elements.asp)
- [The Accessibility Cheatsheet](https://bitsofco.de/the-accessibility-cheatsheet/)
- [Flexbugs](https://github.com/philipwalton/flexbugs)
- [Using CSS Flexible Boxes](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Using_CSS_flexible_boxes)
- [Responsive Fonts](https://websemantics.uk/articles/responsive-fonts/demo/)
